import { Component, OnInit,Injectable } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { PostArrayService } from '../service/PostArray.service';

@Component({
  selector: 'app-NewPost',
  templateUrl: './NewPost.component.html',
  styleUrls: ['./NewPost.component.css']
})
export class NewPostComponent implements OnInit {
  addForm:FormGroup;

  constructor(private fb:FormBuilder,private ListPost:PostArrayService) { }
  
  ngOnInit() {
    this.verifyPostForm();
  }

  verifyPostForm() {
    this.addForm = this.fb.group({
      title: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      message: ['', [Validators.required, Validators.minLength(10)]],
    })
  }

  add(){
    if(this.addForm.valid){
      this.ListPost.addPost(this.addForm);
      this.addForm.reset();
    }
   
    
  }

}
