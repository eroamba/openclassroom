import { Component, OnInit, Input } from '@angular/core';
import { PostArrayService } from 'src/app/service/PostArray.service';

@Component({
  selector: 'app-PostListItem',
  templateUrl: './PostListItem.component.html',
  styleUrls: ['./PostListItem.component.css']
})
export class PostListItemComponent implements OnInit {
 @Input() titre:string;
 @Input() heure:Date;
 @Input() contenu:string;
 @Input() value:number;
 @Input() indexNumber:number;

  constructor(private BlogArray:PostArrayService) { }
 love(){
   this.value+=1;
   console.log(this.value);
 }
 dontLove(){
   this.value-=1;
   console.log(this.value);
 }
  ngOnInit() {
    
  }
  removeItem(val:number){
    this.BlogArray.ArryPosts.splice(val,1);
  }

}
