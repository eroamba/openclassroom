import { Component, OnInit, Input } from '@angular/core';
import { PostArrayService } from '../service/PostArray.service';

@Component({
  selector: 'app-PostList',
  templateUrl: './PostList.component.html',
  styleUrls: ['./PostList.component.css']
})
export class PostListComponent implements OnInit {

  BlogPostArray:any[];

  constructor(private BlogArray:PostArrayService) { }

  ngOnInit() {
    this.BlogPostArray=this.BlogArray.ArryPosts;
  }

}
