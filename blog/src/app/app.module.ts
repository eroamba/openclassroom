import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PostListComponent } from './PostList/PostList.component';
import { PostListItemComponent } from './PostList/PostListItem/PostListItem.component';
import { NewPostComponent } from './NewPost/NewPost.component';
import {Routes, RouterModule} from '@angular/router';
import { PostArrayService } from './service/PostArray.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


const appRoutes:Routes=[
   {path:'post',component:PostListComponent  },
   {path:'nouveau-post',component:NewPostComponent},
   {path:'',component:PostListComponent}
];

@NgModule({
   declarations: [
      AppComponent,
      PostListComponent,
      PostListItemComponent,
      NewPostComponent,
    
   ],
   imports: [
      BrowserModule,
      RouterModule.forRoot(appRoutes),
      FormsModule,
      ReactiveFormsModule
   ],
   providers: [
      PostArrayService,
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
