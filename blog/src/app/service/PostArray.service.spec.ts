/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PostArrayService } from './PostArray.service';

describe('Service: PostArray', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PostArrayService]
    });
  });

  it('should ...', inject([PostArrayService], (service: PostArrayService) => {
    expect(service).toBeTruthy();
  }));
});
